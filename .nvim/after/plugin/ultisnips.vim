let g:snips_author = "Tom Vincent"
let g:snips_author_email = "https://tlvince.com/contact"
let g:snips_license_mit_url = "http://tlvince.mit-license.org"
