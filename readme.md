# Vim Configuration

Optimised for web development.

## Usage

1. Clone the repository

```bash
git clone --recursive https://github.com/tlvince/vim-config.git $HOME/.vim
```

2. Compile YouCompleteMe

```bash
cd $HOME/.vim
make build-ycm
```

## Thanks

* [Steve Losh][sl] for his setup guide
* [Patrick Brisbin][pb] for his `.vimrc`
* [Vim Casts][vc] for the `git submodule` idea for plugins
* [Vim-Scripts][vs] for the convenient GitHub mirror

  [sl]: http://stevelosh.com/blog/2010/09/coming-home-to-vim/
  [pb]: https://github.com/pbrisbin/dotfiles
  [vc]: http://vimcasts.org/episodes/synchronizing-plugins-with-git-submodules-and-pathogen/
  [vs]: http://vim-scripts.org/

## Author

© 2013 Tom Vincent <http://tlvince.com/contact>

## License

Released under the [MIT license](http://tlvince.mit-license.org).
